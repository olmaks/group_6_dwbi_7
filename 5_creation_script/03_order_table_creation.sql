use videocassette
go

drop table if exists client_order
go

create table client_order (
order_id int NOT NULL PRIMARY KEY IDENTITY,
shipment_id int NOT NULL,
client_id int NOT NULL,
film_id int NOT NULL,
payment_id int NOT NULL,
total_price decimal(10,2) NOT NULL,
order_date datetime NOT NULL default(getdate()),
order_status varchar(20) NOT NULL,
quantity int NOT NULL,
order_finish bit NOT NULL,
comment varchar(50) NULL,
buy bit NOT NULL
)
go