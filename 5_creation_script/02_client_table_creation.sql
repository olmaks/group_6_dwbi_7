use videocassette
go

drop table if exists client
go

create table client(
client_id int NOT NULL PRIMARY KEY,
name varchar(30) not null,
surname varchar(30) not null,
lastname varchar(30) null,
birthday date null,
total_buy int not null,
first_buy varchar(50) not null,
country varchar(30) not null,
city varchar(30) not null,
client_index varchar(10) not null,
street varchar(30) not null,
street_number int not null,
flat_number int null,
discount int null,
phone_number varchar(15) null,
mobile_number varchar(13) not null
)
go
