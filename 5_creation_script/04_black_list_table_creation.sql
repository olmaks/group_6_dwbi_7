use videocassette
go

drop table if exists black_list
go

create table black_list(
black_list_id int NOT NULL PRIMARY KEY IDENTITY,
client_id int NOT NULL,
reason varchar(50) NOT NULL,
block_date datetime NOT NULL default(getdate())
)
go
