use videocassette
go


ALTER TABLE client_order
ADD CONSTRAINT [FK_shipment_id] FOREIGN KEY ([shipment_id])
	REFERENCES [shipment] ([shipment_id])
on delete cascade
on update cascade
go

ALTER TABLE client_order
ADD CONSTRAINT [FK_client_id] FOREIGN KEY ([client_id])
	REFERENCES [client] ([client_id])
on delete cascade
on update cascade
go

ALTER TABLE client_order
ADD CONSTRAINT [FK_film_id] FOREIGN KEY ([film_id])
	REFERENCES [film] ([film_id])
on delete cascade
on update cascade
go

ALTER TABLE client_order
ADD CONSTRAINT [FK_payment_id] FOREIGN KEY ([payment_id])
	REFERENCES [payment] ([payment_id])
on delete cascade
on update cascade
go