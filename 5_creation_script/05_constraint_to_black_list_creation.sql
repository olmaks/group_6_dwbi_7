use videocassette
go

ADD
CONSTRAINT [FK_client_id] FOREIGN KEY ([client_id])
	REFERENCES [client] ([client_id])
on delete cascade
on update cascade
